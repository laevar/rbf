# -*- coding: utf-8 -*-                                                                       
from scipy.special import kv
from numpy import array, tile, dot, newaxis, exp, mod, zeros, log, sqrt, shape, concatenate, size
from numpy.linalg import solve

class RBF(object):
    """
    RBF(**kwargs)
    a class for interpolation on n-dimensional scattered data via k-th derivatives of radial basis
    functions using halved squared euclidian distances as radius.

    Parameters:

    points : array
        array with coordinates of the interpolation points as elements. Each element is a n-1 dimensional array [x, y, ...]

    vals : array
        values of the interpolation points

    function : string, optional
        the radial basis function used for interpolation on the elements of the distance matrix, default: 'gaussian'
        there are different types of callable rbfs:

        ==================================== ====================================
        name                                 function
        ==================================== ====================================
        'gaussian'                           :func:`_gaussian`
        'multiquadratic'                     :func:`_multiquadratic`
        'power'                              :func:`_power`
        'thinplate'                          :func:`_thinplate`
        'maternsobolev'                      :func:`_maternsobolev`
        ==================================== ====================================

        One can also define an own function and use it as parameter. There is already no possibility to derivate the defined function via a class method.
        This still has to be done.


    epsilon : float, optional
        constant, used in :func:`_power`, :func:`_thinplate` and :func:`_maternsobolev` because they have singularities at the origin.
        To avoid this singularities, a small positive constan epsilon is added to the endangered argument. Default: 0.0

    k : int, optional
        the order of the chosen RBF's derivative. The default is 0

    par : float, optional
        the needed Parameter :math:`\\beta` for the functions :func:`_maternsobolev`, :func:`_multiquadratic`, :func:`_power`,
        :func:`_thinplate`.

    scale : float, optional
        scaling factor of the interpolated values. Default: 1.0

    """

    #Distance Matrices
    def __distmat(self,P,Q):
        """
        Create a halved squared euclidian distance matrix of the input point matrices with M and N d-dimensional
        points :math:`P \\in \mathbb{R}^{M \\times d}` and
        :math:`Q \\in \mathbb{R}^{N \\times d}`. The distance Matrix :math:`D` generates its entries via using the equation

        .. math:: D_{ij} =  \\| P^{T}e_i - Q^{T}e_j \\|^{2}_2 = \\| P^{T}e_i \\|^{2}_2 + \\| Q^{T}e_j \\|^{2}_2 - 2e^{T}_{i}PQ^{T}e_{j}

        for :math:`1 \\le i \\le M, 1 \\le j \\le N`.

        Example:

        .. code-block:: python

            >>> ar1 = array([[1.0, 2.0, 3.0],[1.5, 2.5, 3.5],[2.0, 3.0, 4.0]])
            >>> ar2 = array([[1.3, 1.0, 1.0],[1.5, 2.5, 3.5],[2.0, 3.0, 4.0]])
            >>> _RBF__distmat(ar,ar2)
            array([[ 2.545,  0.375,  1.5  ],
                   [ 4.27 ,  0.   ,  0.375],
                   [ 6.745,  0.375,  0.   ]])

        """
        nps,nqs = len(P),len(Q)
        pdim,qdim = P.shape[1],Q.shape[1]
        qst = Q.transpose()
        if pdim != qdim:
            raise ValueError('Error: unequal dimension')
        if pdim == 1:
            dst1 = ((tile(P,(1,nqs))-tile(qst,(nps,1)))**2)/2.
            return dst
        dst1 = dot(P,qst)
        cp = 0.5*(array([sum(m) for m in (P**2)])).transpose() #halved squared norms of P points as row
        cq = 0.5*(array([sum(m) for m in (Q**2)])).transpose() #halved squared norms of Q points as row
        dst1 = tile(cp[newaxis,:].transpose(),(1,nqs))+tile(cq,(nps,1))-dst1
        return dst1


    def _gaussian(self,s,k):   #Gaussian
        """
        returns the k-th derivative of the Gaussian function :math:`f(s) = e^{-s}`
        """
        if mod(k,2) == 0:
            return exp(-(1.0*s))
        else:
            return -(1.0)*exp(-(1.0*s))

    def _multiquadratic(self,s,k):          #k-th Derivative of f(s) = (1+2*s)^(par/2)
        """
        returns the k-th derivative of the multiquadratic function :math:`f_{\\beta}(s) = (1+2s)^{\\beta/2}`
        for :math:`\\beta \\notin 2\mathbb{N}_0`, so inverse multiquadratic is possible.
        """
        fac = 1
        ordn = k
        par1 = self.par
        while ordn > 0:
            ordn = ordn - 1
            fac = fac * par1
            par1 = par1 - 2
        return fac*(1+2*s)**(par1/2.)

    def _power(self,s,k):
        """
        returns the k-th derivative of Power function :math:`f(s) = (2s + \\epsilon)^{\\beta/2}`
        with :math:`\\beta > 0, \\beta \\notin 2\mathbb{Z}`.
        """             #k-th Derivative of Power Function f(s) = (2*s + eps)^(par/2)
        fac = 1.
        ordn = k
        par1 = self.par
        while ordn > 0:
            ordn = ordn - 1
            fac = fac * par1
            par1 = par1 - 2
        return fac * ( 2 * s + self.eps )**(par1/2.)

    def _thinplate(self,s,k):
        """
        returns thew k-th derivative of thin-plate spline function :math:`f(s) = (2s + \\epsilon)^{\\beta/2}log(\sqrt{2s + \\epsilon})`
        with :math:`\\beta \\in 2\mathbb{Z}.`
        """
        fac = 1
        ordn = k
        par1 = self.par
        su = 0
        while ordn > 0:
            ordn = ordn-1
            if ordn == k-1:
                su = 1
            else:
                su = su*par1+fac
            fac = fac*par1
            par1 = par1-2
        y = (2*s+self.eps)**(par1/2.)
        y = (fac*y)*log(2*s+self.eps)/2.+su*y
        return y

    def _maternsobolev(self,s,k):
        """
        returns the Matern-Sobolev function :math:`f_{\\nu}(s) = K_{\\nu}(\sqrt{2s + \\epsilon})(\sqrt{2s + \\epsilon})^{\\nu}`.

        :math:`K_{\\nu}(s)` ist the Bessel Function of second kind.

        """ ## k-th Derivative of f(s) = bk(par,sqrt(2*s+eps))*sqrt(2*s+eps)**par with bk(n,z),
                                  ## the Bessel function of second kind
        return (-1)**k*kv(self.par-k,sqrt(2*s+self.eps))*(sqrt(2*s+self.eps))**(self.par-k)

    def getdistmat(self,pr,qr):
        """
        call distmat
        """
        return self.__distmat(pr,qr)

    def __init__(self,**kwargs):
        """
        Gets the arguments and creates a distance matrix :math:`D` of :math:`points`, if there are points in the arguments. Applys the chosen radial
        basis function on the distance matrix
        and generates the coefficient array :math:`a` by solving the equation

        .. math:: \sum_{j=1}^M D_{kj}a_j = v_k

        with :math:`v` the array of values and :math:`1 \\le k \\le M`.
        """

        #kwargs: scale, par, k, eps
        self.interpolationpoints = kwargs.pop('points', None)
        self.interpolationvals = kwargs.pop('values', None)
        self.rbfscale = kwargs.pop('scale', 1.0)
        self.par = kwargs.pop('par', None)
        self.kord = kwargs.pop('k', 0)
        self.eps = kwargs.pop('eps', 0.0)
        self.functionc = kwargs.pop('function','gaussian')
        if isinstance(self.functionc, str):
            self.frbf = self.__getattribute__("_" + self.functionc)
            self.isown = True
            if self.interpolationpoints is None:
                pass
            else:
                self.dst = self.__distmat(self.interpolationpoints, self.interpolationpoints)
                self.evalmat = self.frbf(self.dst / (self.rbfscale**2),self.kord)
                self.coeffs = solve(self.evalmat, self.interpolationvals)
        else:
            if hasattr(self.functionc, '__call__'):
                self.isown = False
                self.frbf = self.functionc
                if self.interpolationpoints is None:
                    pass
                else:
                    self.dst = self.__distmat(self.interpolationpoints, self.interpolationpoints)
                    self.evalmat = self.frbf(self.dst / (self.rbfscale**2))
                    self.coeffs = solve(self.evalmat, self.interpolationvals)



    def __call__(self, newpoints):
        """
        creates a new distance matrix :math:`M` between `newpoints` and the interpolation points.
        returns the product of the new distance Matrix and the coefficient array, i.e. the new reconstructed values of the newpoints as an array.
        """
        self.newdst = self.__distmat(newpoints, self.interpolationpoints)
        if self.isown is True:
            return dot(self.frbf(self.newdst/(self.rbfscale**2),self.kord), self.coeffs)
        else:
            return dot(self.frbf(self.newdst/(self.rbfscale**2)), self.coeffs)
