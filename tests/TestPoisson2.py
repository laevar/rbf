#-------------------------------------------------------------------------------
# Name:        TestPoisson
# Purpose:     Test the RBF-class on the Poisson-problem via Kansa's straight
#              collocation method.
#
# Author:      Joachim
#
# Created:     19.08.2015
# Copyright:   (c) Joachim 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from RBF import *


def u4(x,y):
    return np.exp((x-0.1)**2+0.5*y**2)/np.exp(1.21)

def lapu4(x,y):
    return (5*np.exp((x.transpose()-0.1)**2+0.5*y.transpose()**2)/np.exp(1.21))

#interpolation points


def pm3(x,y,z):
    return np.array([(x[mi][m1],y[mi][m1],z[mi][m1]) for mi in range(len(x)) for m1 in range(len(x[mi]))])


#generate points
newradius = np.sqrt(np.random.random((90,)));
newwinkel = 2* np.pi * np.random.random((90,))
arrcoordi = np.array([(newradius[m]*np.cos(newwinkel[m]),newradius[m]*np.sin(newwinkel[m])) for m in range(len(newwinkel))])
arrwinkel = 0.0 + (2*np.pi)*np.random.random((40,))

#point matrices
pminterpolatei = np.array([(newradius[m]*np.cos(newwinkel[m]),newradius[m]*np.sin(newwinkel[m])) for m in range(len(newwinkel))])
pminterpolateb = np.array([(np.cos(n),np.sin(n)) for n in arrwinkel])
valinterpolateb = u4(pminterpolateb[:,0],pminterpolateb[:,1])
valinterpolatei = lapu4(pminterpolatei[:,0],pminterpolatei[:,1])

pminterpolateall = np.concatenate((pminterpolateb,pminterpolatei))
valinterpolateall = np.concatenate((valinterpolateb,valinterpolatei))


#evaluation points
pmevaluatei = np.array([(m*np.cos(n), m*np.sin(n)) for n in np.arange(0.,2*np.pi, 2*np.pi/40.) for m in np.arange(0.,1.0,0.1)])
pmevaluateb = np.array([(np.cos(n),np.sin(n)) for n in np.arange(0.,2*np.pi, 2*np.pi/40.)])
valevaluate = np.concatenate((u4(pmevaluateb[:,0],pmevaluateb[:,1]),lapu4(pmevaluatei[:,0],pmevaluatei[:,1])))

#generate RBF
prbf2 = RBF(function ='power', par = 5.0)
print(prbf2.frbf(0.5,0))

pmevaluateall = np.concatenate((pmevaluateb,pmevaluatei))

#distance matrices
distmati = prbf2.getdistmat(pminterpolatei,pminterpolateall)
distmatb = prbf2.getdistmat(pminterpolateb,pminterpolateall)
evalmatb = prbf2.frbf(distmatb,0)
dop, dip = pminterpolatei.shape
evalmati = (dip*prbf2.frbf(distmati,1)+2*distmati*prbf2.frbf(distmati,2))
evalmatall = np.concatenate((evalmatb,evalmati))
coeffs = np.linalg.solve(evalmatall, valinterpolateall)


distmatevali = prbf2.getdistmat(pmevaluatei, pminterpolateall)
distmatevalb = prbf2.getdistmat(pmevaluateb, pminterpolateall)

newdip = pmevaluatei.shape[1]
newevalmatb = prbf2.frbf(distmatevalb,0)
newevalmati = prbf2.frbf(distmatevali,0)
newevalmat = np.concatenate((newevalmatb,newevalmati))
newvals = np.dot(newevalmat, coeffs)

fig = plt.figure()
ax = fig.add_subplot(221)
ax.set_title('interpolation points')
ax.plot(arrcoordi[:,0], arrcoordi[:,1], '.', color = 'blue')
ax.plot(pminterpolateb[:,0],pminterpolateb[:,1], '.', color = 'red')
ax = fig.add_subplot(222, projection ='3d')
ax.set_title('given function')
ax.plot_trisurf(pminterpolateall[:,0],pminterpolateall[:,1],u4(pminterpolateall[:,0],pminterpolateall[:,1]), cmap=cm.seismic)
ax = fig.add_subplot(223, projection ='3d')
ax.set_title('reconstructed function')
ax.plot_trisurf(pmevaluateall[:,0],pmevaluateall[:,1],newvals,cmap=cm.seismic)
ax = fig.add_subplot(224, projection ='3d')
ax.set_title('error')
ax.plot_trisurf(pmevaluateall[:,0],pmevaluateall[:,1],u4(pmevaluateall[:,0],pmevaluateall[:,1])-newvals)
plt.show()
