from RBF import RBF
import numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from matplotlib.mlab import  bivariate_normal
from matplotlib import cm


#generating a point Matrix with 3 columns
def pm3(x,y,z):
    return np.array([(x[mi][m1],y[mi][m1],z[mi][m1]) for mi in range(len(x)) for m1 in range(len(x[mi]))])


#generate random interpolation points with function values (bivariate normal)
np1 = 10
pr = 6*np.random.rand(np1*np1,2)-3
X,Y = np.meshgrid(pr[:,0],pr[:,1])
Z1 = bivariate_normal(X, Y, 1.0, 1.0, 0.0, 0.0)
Z2 = bivariate_normal(X, Y, 1.5, 0.5, 1, 1)
Z = Z2 - Z1
pointsz = pm3(X,Y,Z)

#initialize RBF and interpolate
rbfi = RBF(points = pointsz[:,:2], values = pointsz[:,2], function='gaussian', scale = 0.5)


#generate meshgrid and values for given function
XE,YE = np.meshgrid(np.arange(-3.0,3.0,0.06),np.arange(-3.0,3.0,0.06))
ZE1 = bivariate_normal(XE, YE, 1.0, 1.0, 0.0, 0.0)
ZE2 = bivariate_normal(XE, YE, 1.5, 0.5, 1, 1)
ZE = ZE2 - ZE1


#reconstruct function values
vals1 = rbfi(pm3(XE,YE,ZE)[:,:2])


#plot
fig = plt.figure()
ax = fig.add_subplot(221)
plt.plot(pr[:,0],pr[:,1], '.')
ax.set_title('interpolation points')
ax = fig.add_subplot(222, projection='3d')
ax.set_title('given function')
ax.plot_surface(XE, YE, ZE, cmap = cm.jet)
ax = fig.add_subplot(223, projection='3d')
ax.set_title('reconstructed function')
ax.plot_surface(XE, YE, np.reshape(vals1,np.shape(XE)),cmap=cm.jet)
ax = fig.add_subplot(224, projection='3d')
ax.set_title('error')
ax.plot_surface(XE, YE, ZE - np.reshape(vals1,np.shape(XE)))
plt.show()
