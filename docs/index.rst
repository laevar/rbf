.. RBF documentation master file, created by
   sphinx-quickstart on Thu Aug 13 14:51:24 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. include:: ../README.rst

Contents:

.. toctree::
   :maxdepth: 2

   tutorial
   code

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

