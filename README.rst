Welcome to RBF's documentation!
===================================

Radial Basis Functions (RBF) are used to interpolate scattered data. This class can be used for 'typical' RBF-interpolation and has possibilities for solving PDEs with RBFs.
The implementation is based on the Paper 'MATLAB-Programming for Kernel-Based Methods' by Robert Schaback. The standard RBF-types are implemented. 

Requirements:

RBF depends on the numpy and scipy package.

